﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DataLayer
{
    public class Database
    {
        
        static string connectionString = "Data Source=LCI-PC;Initial Catalog=SimpleMessagingSystemDB;Integrated Security=True";
        static SqlConnection connection = new SqlConnection(connectionString);

        public static void Main()
        {}
        
#region handling user methods 

        /// <summary>
        /// Register user and wait for an admin to confirm registration.
        /// Return True if user is successfully added
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        public static bool InsertUser(User user)
        {
            int count = 0;
            // insert
            string query = string.Format(@"INSERT INTO dbo.UserPendingApproval (UserName, Salt, Password, FirstName, LastName, BirthDate, Gender) 
                VALUES (@username, @binarySalt, @binaryPassword,@firstName,@lastName,@date,@gender)");
           
            //check if this username already exists
            if (!CheckUsernameAvailability(user.Username))
                return false;
            // is ok this username is not used.
            try
           {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = user.Username;
                command.Parameters.Add("@binarySalt", SqlDbType.Binary, 4).Value= user.Salt;
                command.Parameters.Add("@binaryPassword", SqlDbType.Binary, 64).Value = user.Password;
                command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = user.FirstName;
                command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = user.LastName;
                command.Parameters.Add("@date", SqlDbType.DateTime).Value = user.Date;
                command.Parameters.Add("@gender", SqlDbType.TinyInt).Value = user.Gender;
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error insert user" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
           return count == 0 ? false : true;
        }

        /// <summary>
        /// Checks if a username is available to use.
        /// Useful when someone is registering.
        /// </summary>
        /// <param name="username">username to check</param>
        /// <returns>Return a bool value, true if is available false otherwise</returns>
        public static bool CheckUsernameAvailability(String username)
        {
            int count = 0;
            try
            {
                connection.Open();
                string query = string.Format(@"SELECT Count(*) AS countUsers 
                                            FROM dbo.UserData WHERE UserName='{0}'", username);
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        count = Convert.ToInt32(reader["countUsers"]);
                    }
                }
                command.Dispose();
            }
            catch
            {
                Console.WriteLine("Error CheckUsernameAvailability");
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? true : false;
        }

        /// <summary>
        /// Return all users that are awaiting admin approval.
        /// </summary>
        /// <returns></returns>
        public static List<User> GetUserRequestsConfirmation()
        {
            List<User> unconfirmedUsers = new List<User>();
            string query = string.Format("SELECT * FROM  dbo.UserPendingApproval");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        unconfirmedUsers.Add(new User(Convert.ToInt32(reader["UserId"].ToString()),
                                                    reader["UserName"].ToString(), 
                                                    (byte[])reader["Salt"],
                                                    (byte[])reader["Password"],
                                                    reader["FirstName"].ToString(),
                                                    reader["LastName"].ToString(), 
                                                    Convert.ToDateTime(reader["BirthDate"]),
                                                    (Gender)Convert.ToByte(reader["Gender"]), 
                                                    reader["Role"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetUserRequestsConfirmation" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return unconfirmedUsers;
        }

        /// <summary>
        /// Approve user registration request
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns></returns>
        public static bool ConfirmUser(int id)
        {
            User user = GetUnconfirmedUserById(id);
            return ConfirmUser(user);
        }

        /// <summary>
        /// Approve user registration request
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static bool ConfirmUser(User user)
        {
            int count = 0;
            // insert
            string query = @"INSERT INTO dbo.UserData (UserName, Salt, Password, FirstName, LastName, BirthDate, Gender, Role) 
                            VALUES (@username, @binarySalt, @binaryPassword,@firstName,@lastName,@date,@gender,@role)";
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = user.Username;
                command.Parameters.Add("@binarySalt", SqlDbType.Binary, 4).Value = user.Salt;
                command.Parameters.Add("@binaryPassword", SqlDbType.Binary, 64).Value = user.Password;
                command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = user.FirstName;
                command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = user.LastName;
                command.Parameters.Add("@date", SqlDbType.DateTime).Value = user.Date;
                command.Parameters.Add("@gender", SqlDbType.TinyInt).Value = user.Gender;
                command.Parameters.Add("@role", SqlDbType.NChar, 5).Value = user.Role;
                count = command.ExecuteNonQuery();
                command.Dispose();
                query = string.Format("DELETE FROM dbo.UserPendingApproval WHERE UserId={0}",user.Id);
                command = new SqlCommand(query, connection);
                command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error confirm user" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
        }

        /// <summary>
        /// Deny registration request
        /// </summary>
        /// <param name="id">user id</param>
        /// <returns></returns>
        public static bool DenyUser(int id)
        {
            string query = string.Format("DELETE FROM dbo.UserPendingApproval WHERE UserId={0}",id);
            int count = 0;
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error deny user" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
        }

        public static bool UpdateUser(User user) 
        {
            int count = 0;
            string query = @"UPDATE dbo.UserData 
                            SET UserName=@username, Salt=@salt, Password=@password, FirstName=@firstName, 
                                LastName=@lastName, BirthDate=@birthDate, Gender=@gender, Role=@role
                            WHERE UserId=@userId";
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@userId", SqlDbType.Int).Value = user.Id;
                command.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = user.Username;
                command.Parameters.Add("@salt", SqlDbType.Binary, 4).Value = user.Salt;
                command.Parameters.Add("@password", SqlDbType.Binary, 64).Value = user.Password;
                command.Parameters.Add("@firstName", SqlDbType.NVarChar, 50).Value = user.FirstName;
                command.Parameters.Add("@lastName", SqlDbType.NVarChar, 50).Value = user.LastName;
                command.Parameters.Add("@birthDate", SqlDbType.DateTime).Value = user.Date;
                command.Parameters.Add("@gender", SqlDbType.TinyInt).Value = user.Gender;
                command.Parameters.Add("@role", SqlDbType.NChar, 5).Value = user.Role;
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error update user" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
            
        }

        /// <summary>
        /// Change user role
        /// </summary>
        /// <param name="userId">user id</param>
        /// <param name="newRole">new role</param>
        /// <returns></returns>
        public static bool UpdateUserRole(int userId, string newRole)
        {
            User user = GetUserById(userId);
            user.Role = newRole;
            return UpdateUser(user);
        }

        /// <summary>
        /// Checks if a user has a specific role
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="role">role to be checked</param>
        /// <returns></returns>
        public static bool CheckUserRole(string username, string role)
        {
            int count = 0;
            try
            {
                connection.Open();
                string query = string.Format(@"SELECT Count(*)AS countUsers 
                                               FROM dbo.UserData 
                                               WHERE UserName='{0}' and role='{1}'",username, role);
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        count = Convert.ToInt32(reader["countUsers"]);
                    }
                }
                command.Dispose();
            }
            catch
            {
                Console.WriteLine("Error CheckUserRole");
            }
            finally
            {
                connection.Close();
            }
            return count == 1 ? true : false;
        }

        /// <summary>
        /// Delete a user on the basis of id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteUser(int id)
        {
            string query = string.Format("DELETE FROM dbo.UserData WHERE UserId={0}",id);
            int count = 0;
            try
            {
                connection.Open();
                //delete messages - this operation is necessary because tables are linked in database
                string queryDelMsgs = string.Format(@"DELETE FROM dbo.Message 
                                                      WHERE [To]={0} OR [From]={0}", id);
                SqlCommand command = new SqlCommand(queryDelMsgs, connection);
                command.ExecuteNonQuery();
                //delete user
                command = new SqlCommand(query, connection);
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error delete user" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
        }

        /// <summary>
        /// Checks input credentials for login.
        /// Return True or False. 
        /// If credentials are correct a user object is passed, otherwise null is passed.
        /// </summary>
        /// <param name="username">username</param>
        /// <param name="passwordInClear">password in clear</param>
        /// <param name="user">output object</param>
        /// <returns>bool value</returns>
        public static bool CheckCredentials(string username, string passwordInClear , out User user)
        {
            User userDB= null;
            //get user with this name from DB
            string query =string.Format("SELECT TOP 1 *  FROM dbo.UserData WHERE UserName='{0}'",username);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {   //create user object to return
                        userDB = new User(Convert.ToInt32(reader["UserId"].ToString()), 
                                          reader["UserName"].ToString(), 
                                          (byte[])reader["Salt"],
                                          (byte[])reader["Password"], 
                                          reader["FirstName"].ToString(),
                                          reader["LastName"].ToString(), 
                                          Convert.ToDateTime(reader["BirthDate"]),
                                          (Gender)Convert.ToByte(reader["Gender"]), 
                                          reader["Role"].ToString());
                    }
                }
            }
            catch(Exception ex)
            {
                Console.WriteLine("Error in CheckCredentials" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }

            //check password
            if (userDB != null && userDB.CheckPassword(passwordInClear))
            {
                user = userDB; 
                return true;
            }
            else
            {
                user = null;
                return false;
            }          
        }

        /// <summary>
        /// Get all registered and confirmed users.
        /// </summary>
        /// <returns></returns>
        public static List<User> GetAllUsers()
        {
            List<User> users = new List<User>();
            string query = string.Format("SELECT * FROM  dbo.UserData");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        users.Add(new User(Convert.ToInt32(reader["UserId"].ToString()), 
                                           reader["UserName"].ToString(), 
                                           (byte[])reader["Salt"],
                                           (byte[])reader["Password"], 
                                           reader["FirstName"].ToString(),
                                           reader["LastName"].ToString(), 
                                           Convert.ToDateTime(reader["BirthDate"]),
                                           (Gender)Convert.ToByte(reader["Gender"]), 
                                           reader["Role"].ToString()));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetAllUsers"+ ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return users;
        }

        /// <summary>
        /// Retrieve user based on id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User GetUserById(int id)
        {
            User user = null;
            string query = string.Format("SELECT * FROM  dbo.UserData WHERE UserId={0} ",id);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        user = new User(Convert.ToInt32(reader["UserId"].ToString()), 
                                        reader["UserName"].ToString(), 
                                        (byte[])reader["Salt"],
                                        (byte[])reader["Password"], 
                                        reader["FirstName"].ToString(), 
                                        reader["LastName"].ToString(), 
                                        Convert.ToDateTime(reader["BirthDate"]),
                                        (Gender)Convert.ToByte(reader["Gender"]), 
                                        reader["Role"].ToString());   
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetUserById" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return user;
        }

        /// <summary>
        /// Get unconfirmed user based on id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static User GetUnconfirmedUserById(int id)
        {
            User user = null;
            string query = string.Format(@"SELECT * FROM  dbo.UserPendingApproval 
                                           WHERE UserId={0} ", id);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        user = new User(Convert.ToInt32(reader["UserId"].ToString()), 
                                        reader["UserName"].ToString(), 
                                        (byte[])reader["Salt"],
                                        (byte[])reader["Password"], 
                                        reader["FirstName"].ToString(),
                                        reader["LastName"].ToString(),
                                        Convert.ToDateTime(reader["BirthDate"]),
                                        (Gender)Convert.ToByte(reader["Gender"]), 
                                        reader["Role"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetUnconfirmedUserById" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return user;
        }

        /// <summary>
        /// Get user by username
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static User GetUserByUsername(string username)
        {
            User user = null;
            string query = string.Format("SELECT * FROM  dbo.UserData WHERE Username='{0}' ", username);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        user = new User(Convert.ToInt32(reader["UserId"].ToString()), 
                                        reader["UserName"].ToString(), 
                                        (byte[])reader["Salt"],
                                        (byte[])reader["Password"], 
                                        reader["FirstName"].ToString(), 
                                        reader["LastName"].ToString(),
                                        Convert.ToDateTime(reader["BirthDate"]),
                                        (Gender)Convert.ToByte(reader["Gender"]), 
                                        reader["Role"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetUserByUsername" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return user;
        }

        /// <summary>
        /// Get user role.
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static string GetUserRole(string username)
        {
            string query = string.Format(@"SELECT TOP 1 Role FROM  dbo.UserData 
                                            WHERE Username='{0}' ", username);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        return reader["Role"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetUserRole"+ ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return null;
        }

        /// <summary>
        /// Get number of confirmed users.
        /// </summary>
        /// <returns></returns>
        public static int GetNumberOfUsers()
        {
            int nrOfUsers = 0;
            string query = string.Format(@"SELECT Count(UserId) AS Users 
                                            FROM  dbo.UserData");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        nrOfUsers = Convert.ToInt32(reader["Users"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetNumberUsers"+ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return nrOfUsers;
        }

        /// <summary>
        /// Get number of unconfirmed users.
        /// </summary>
        /// <returns></returns>
        public static int GetNumberOfUnconfirmedUsers()
        {
            int nrOfUsers = 0;
            string query = string.Format(@"SELECT Count(UserId) AS Users 
                                            FROM  dbo.UserPendingApproval");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        nrOfUsers = Convert.ToInt32(reader["Users"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetNumberUnconfirmedUsers"+ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return nrOfUsers;
        }
#endregion
#region handling message methods 

        /// <summary>
        /// Add message in database.
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        public static bool AddMessage(Message msg)
        {
            int count = 0;

            string query = @"INSERT INTO dbo.Message ([From], [To], Subject, SentDate, ReceivedDate, Message , [Read]) 
                VALUES (@from, @to, @subject,@sentDate,@receivedDate,@message, @read)";

            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@from", SqlDbType.Int).Value = msg.From;
                command.Parameters.Add("@to", SqlDbType.Int).Value = msg.To;
                command.Parameters.Add("@subject", SqlDbType.NVarChar, 50).Value = msg.Subject;
                command.Parameters.Add("@sentDate", SqlDbType.DateTime).Value = msg.SentDate;
                command.Parameters.Add("@receivedDate", SqlDbType.DateTime).Value = msg.ReceivedDate;
                command.Parameters.Add("@message", SqlDbType.NVarChar, -1 ).Value = msg.MessageValue;
                command.Parameters.Add("@read", SqlDbType.Bit).Value = msg.Read;
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error add message" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
        }

        /// <summary>
        /// Get message by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static Message GetMessageById(int id)
        {
            Message message = null;
            string query = string.Format("SELECT * FROM  dbo.Message WHERE MessageId={0} ", id);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        message = new Message(Convert.ToInt32(reader["MessageId"]),
                                Convert.ToInt32(reader["From"]),
                                Convert.ToInt32(reader["To"]),
                                reader["Subject"].ToString(),
                                Convert.ToDateTime(reader["SentDate"]),
                                Convert.ToDateTime(reader["ReceivedDate"]),
                                reader["Message"].ToString(),
                                Convert.ToBoolean(reader["Read"]));

                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetMessageById"+ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return message;
        }

        /// <summary>
        /// Get all messages.
        /// </summary>
        /// <returns></returns>
        public static List<Message> GetAllMessages()
        {
            List<Message> msgs = new List<Message>();
            string query = @"SELECT * FROM dbo.Message ORDER BY SentDate Desc";
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        msgs.Add(new Message(Convert.ToInt32(reader["MessageId"]),
                                Convert.ToInt32(reader["From"]),
                                Convert.ToInt32(reader["To"]),
                                reader["Subject"].ToString(),
                                Convert.ToDateTime(reader["SentDate"]),
                                Convert.ToDateTime(reader["ReceivedDate"]),
                                reader["Message"].ToString(),
                                Convert.ToBoolean(reader["Read"]))); 
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Get All Message"+ ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return msgs;
        }

        /// <summary>
        /// Get sent messages by a user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Message> GetSentMessages(User user)
        {
            List<Message> msgs = new List<Message>();
            string query = @"SELECT * FROM dbo.Message WHERE [From]=@userId ORDER BY SentDate Desc";
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@userId", SqlDbType.Int).Value = user.Id;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        msgs.Add(new Message(Convert.ToInt32(reader["MessageId"]),
                                Convert.ToInt32(reader["From"]),
                                Convert.ToInt32(reader["To"]),
                                reader["Subject"].ToString(),
                                Convert.ToDateTime(reader["SentDate"]),
                                Convert.ToDateTime(reader["ReceivedDate"]),
                                reader["Message"].ToString(),
                                Convert.ToBoolean(reader["Read"])));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Sent Message" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return msgs;
        }

        /// <summary>
        /// Get received messages by a user.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public static List<Message> GetReceivedMessages(User user)
        {
            List<Message> msgs = new List<Message>();
            string query = @"SELECT * FROM dbo.Message WHERE [To]=@userId ORDER BY SentDate Desc";
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@userId", SqlDbType.Int).Value = user.Id;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        msgs.Add(new Message(Convert.ToInt32(reader["MessageId"]),
                                Convert.ToInt32(reader["From"]),
                                Convert.ToInt32(reader["To"]),
                                reader["Subject"].ToString(),
                                Convert.ToDateTime(reader["SentDate"]),
                                Convert.ToDateTime(reader["ReceivedDate"]),
                                reader["Message"].ToString(),
                                Convert.ToBoolean(reader["Read"])));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error Received Message" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return msgs;
        }

        /// <summary>
        /// Mark a message as read
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool MarkMessageAsRead(int id)
        {
            int count = 0;
            string query = @"UPDATE dbo.Message 
                            SET [Read]=@read
                            WHERE MessageId=@messageId";
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.Add("@read", SqlDbType.Bit).Value = 1;
                command.Parameters.Add("@messageId", SqlDbType.Int).Value = id;
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error MarkMessageAsRead" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
        }

        /// <summary>
        /// Get number on sent messages by a user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetNumberOfSentMsgs(int userId)
        {
            int nrOfMsgs = 0;
            string query = string.Format(@"SELECT Count(MessageId) AS NrOfMsgs FROM  dbo.Message 
                                            WHERE [From]={0} ", userId);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        nrOfMsgs = Convert.ToInt32(reader["NrOfMsgs"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetNumberOfSentMsgs" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return nrOfMsgs;
        }

        /// <summary>
        /// Get number on received messages by a user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetNumberOfReceivedMsgs(int userId)
        {
            int nrOfMsgs = 0;
            string query = string.Format(@"SELECT Count(MessageId) AS NrOfMsgs FROM  dbo.Message 
                                            WHERE [To]={0} ", userId);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        nrOfMsgs = Convert.ToInt32(reader["NrOfMsgs"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetNumberOfReceivedMsgs" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return nrOfMsgs;
        }

        /// <summary>
        /// Get number on unread messages by a user.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public static int GetNumberOfUnreadMsgs(int userId)
        {
            int nrOfMsgs = 0;
            string query = string.Format(@"SELECT Count(MessageId) AS NrOfMsgs 
                                            FROM  dbo.Message WHERE [To]={0} AND [Read]=0", userId);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        nrOfMsgs = Convert.ToInt32(reader["NrOfMsgs"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetNumberOfUnreadMsgs" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return nrOfMsgs;
        }

        /// <summary>
        /// Get total number of messages.
        /// </summary>
        /// <returns></returns>
        public static int GetNumberOfMessages()
        {
            int nrOfMsgs = 0;
            string query = string.Format(@"SELECT Count(MessageId) AS NrOfMsgs 
                                            FROM  dbo.Message");
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        nrOfMsgs = Convert.ToInt32(reader["NrOfMsgs"]);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error GetNumberOfMessages" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return nrOfMsgs;
        }

        /// <summary>
        /// Delete a message
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static bool DeleteMessage(int id)
        {
            string query = string.Format("DELETE FROM dbo.Message WHERE MessageId={0}", id);
            int count = 0;
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand(query, connection);
                count = command.ExecuteNonQuery();
                command.Dispose();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error delete message" + ex.StackTrace);
            }
            finally
            {
                connection.Close();
            }
            return count == 0 ? false : true;
        }

#endregion

    }
}
