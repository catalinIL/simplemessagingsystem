﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace DataLayer
{
    /// <summary>
    /// Class reprezenting user entity from database.
    /// </summary>
    public class User
    {
        int id;        
        string username;        
        byte[] salt;
        byte[] password;
        string firstName;
        string lastName;
        DateTime date;
        Gender gender;
        string role;

        public User(int id,string username, byte[] salt, byte[] password, string firstName, string lastName, DateTime date, Gender gender, string role="user")
        {
            this.id = id;
            this.username = username;
            this.salt = salt;
            this.password = password;
            this.firstName = firstName;
            this.lastName = lastName;
            this.date = date;
            this.gender = gender;
            this.role = role;
        }

        public User(string username, string passwordInClear, string firstName, string lastName, DateTime date, Gender gender, string role = "user")
        {
            this.username = username;
            
            #region build password
            // create a salt
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            byte[] salt = new byte[4];
            rng.GetBytes(salt);

            // compute hash password and salt
            byte[] passwordByteArray = ASCIIEncoding.ASCII.GetBytes(passwordInClear);
            byte[] passwordHash = new SHA512CryptoServiceProvider().ComputeHash(passwordByteArray.Concat(salt).ToArray());

            #endregion
            this.salt = salt;
            this.password = passwordHash;
            this.firstName = firstName;
            this.lastName = lastName;
            this.date = date;
            this.gender = gender;
            this.role = role;
        }

        /// <summary>
        /// Checks if an input string is the correct password for this user.
        /// </summary>
        /// <param name="passwordInClear"></param>
        /// <returns></returns>
        public bool CheckPassword(string passwordInClear)
        {
            byte[] passwordByteArray = ASCIIEncoding.ASCII.GetBytes(passwordInClear);
            byte[] passwordHash = new SHA512CryptoServiceProvider().ComputeHash(passwordByteArray.Concat(salt).ToArray());

            if (password.SequenceEqual(passwordHash))
                return true;
            else
                return false;
        }

        #region setters and getters
        public int Id
        {
            get { return id; }
        }
        public string Username
        {
            get { return username; }
            set { username = value; }
        }
        public byte[] Salt
        {
            get { return salt; }
            set { salt = value; }
        }
        public byte[] Password
        {
            get { return password; }
            set { password = value; }
        }
        public string FirstName
        {
            get { return firstName; }
            set { firstName = value; }
        }
        public string LastName
        {
            get { return lastName; }
            set { lastName = value; }
        }
        public DateTime Date
        {
            get { return date; }
            set { date = value; }
        }
        public Gender Gender
        {
            get { return gender; }
            set { gender = value; }
        }
        public string Role
        {
            get { return role; }
            set { role = value; }
        }
        public string FullName 
        {
            get { return this.FirstName + " " + this.LastName; } 
        }
        #endregion

        public override string ToString()
        {
            return id.ToString() + " " + username.ToString() + " " + salt.ToString() + " " + password + " " +
                firstName +" "+lastName + " " + date.ToString() + " " + gender.ToString() + " " + role;
        }
    }

    public enum Gender : byte { M=1,F=2};

    public sealed class Role
    {
        public const string UserRole = "user";
        public const string AdminRole = "admin";
    }
}
