﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DataLayer
{
    /// <summary>
    /// Class reprezenting message entity from database.
    /// </summary>
    public class Message
    {
        int messageId;
        int from;
        int to;
        string subject;
        DateTime sentDate;
        DateTime receivedDate;
        string messageValue;
        bool read;
        private int p1;
        private int p2;
        private DateTime dateTime;
        private string message;

        public Message(int id)
        {
            this.messageId = id;
        }

        public Message(int id, int from, int to, string subject, DateTime sentDate, DateTime receivedDate, string msg , bool read=false)
        {
            this.messageId = id;
            this.from = from;
            this.to = to;
            this.subject = subject;
            this.sentDate = sentDate;
            this.receivedDate = receivedDate;
            this.messageValue = msg;
            this.read = read;
        }

        public Message(int from, int to, string subject, DateTime sentDate, DateTime receivedDate, string msg, bool read = false)
        {
            this.from = from;
            this.to = to;
            this.subject = subject;
            this.sentDate = sentDate;
            this.receivedDate = receivedDate;
            this.messageValue = msg;
            this.read = read;
        }

        public Message(int from, int to, string subject, DateTime sentDate, string message)
        {
            this.from = from;
            this.to = to;
            this.subject = subject;
            this.sentDate = sentDate;
            this.receivedDate = DateTime.Now;
            this.messageValue = message;
            this.read = false;
        }

        public int MessageId
        {
            get { return messageId; }
        }
        public int From
        {
            get { return from; }
            set { from = value; }
        }
        public int To
        {
            get { return to; }
            set { to = value; }
        }
        public string Subject
        {
            get { return subject; }
            set { subject = value; }
        }
        public DateTime SentDate
        {
            get { return sentDate; }
            set { sentDate = value; }
        }
        public DateTime ReceivedDate
        {
            get { return receivedDate; }
            set { receivedDate = value; }
        }
        public string MessageValue
        {
            get { return messageValue; }
            set { messageValue = value; }
        }

        public bool Read
        {
            get { return read; }
            set { read = value; }
        }
        public override string ToString()
        {
            return this.from.ToString() +" "+ this.to.ToString()+" "+this.subject+" "+this.sentDate.ToString();
        }
    }
}
