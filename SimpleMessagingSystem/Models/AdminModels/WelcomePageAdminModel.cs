﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleMessagingSystem.Models.AdminModels
{
    public class WelcomePageAdminModel
    {
        [Display(Name="Number of active users:")]
        public int NumberOfUsers { get; set; }

        [Display(Name = "Number of unconfirmed users:")]
        public int NumberOfUnconfirmedUsers { get; set; }

        [Display(Name = "Number of messages:")]
        public int NumberOfMessages { get; set; }

        public WelcomePageAdminModel(int users, int unconfUsers, int msgs)
        {
            this.NumberOfUsers = users;
            this.NumberOfUnconfirmedUsers = unconfUsers;
            this.NumberOfMessages = msgs;
        }
    }
}