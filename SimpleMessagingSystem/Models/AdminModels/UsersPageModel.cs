﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleMessagingSystem.Models.AdminModels
{
    public class UsersPageModel
    {
        public List<DataLayer.User> UnconfirmedUsers { get; set; }
        public List<DataLayer.User> Users { get; set; }
    }
}