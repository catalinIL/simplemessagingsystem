﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleMessagingSystem.Models.RegularUserModels
{
    public class SentMessageModel
    {
        [Required(ErrorMessage="Please enter a recipient.")]
        [DataType(DataType.Text)]
        [Display(Name="To")]
        public string Users { get; set; }

        [Required(ErrorMessage="Please enter a subject")]
        [DataType(DataType.Text)]
        [Display(Name = "Subject")]
        public string Subject { get; set; }

        [Required(ErrorMessage="The body message is empty.")]
        public string Message { get; set; }
    }
}