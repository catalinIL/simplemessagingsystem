﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleMessagingSystem.Models.RegularUserModels
{
    public class WelcomePageUserModel
    {
        [Display(Name="Number of messages sent:")]
        public int NumberOfMessagesSent { get; set; }

        [Display(Name = "Number of messages received:")]
        public int NumberOfMessagesReceived { get; set; }

        [Display(Name = "Number of unread messages:")]
        public int NumberOfUnreadMessages { get; set; }

        public WelcomePageUserModel(int nrSent, int nrReceived, int nrUnread)
        {
            this.NumberOfMessagesSent = nrSent;
            this.NumberOfMessagesReceived = nrReceived;
            this.NumberOfUnreadMessages = nrUnread;
        }
    }
}