﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SimpleMessagingSystem.Models
{
    /// <summary>
    /// Model for message
    /// </summary>
    public class MessageModel
    {
        public int Id { get; set; }
        public string SenderName { get; set; }
        public string ReceiverName { get; set; }
        public DateTime SentDate { get; set; }
        public DateTime ReceivedDate { get; set; }
        public string Subject { get; set; }
        public string MessageValue { get; set; }
        public bool Read { get; set; }

        public MessageModel()
        { }
        public MessageModel(DataLayer.Message message)
        {
            this.Id = message.MessageId;
            this.SenderName = DataLayer.Database.GetUserById(message.From).FullName;
            this.ReceiverName = DataLayer.Database.GetUserById(message.To).FullName;
            this.SentDate = message.SentDate;
            this.ReceivedDate = message.ReceivedDate;
            this.Subject = message.Subject;
            this.MessageValue = message.MessageValue;
            this.Read = message.Read;
        }
        
        public static List<MessageModel> GetMessageModel(List<DataLayer.Message> messages)
        {
            List<MessageModel> list = new List<MessageModel>();
            foreach (var msg in messages)
            {
                list.Add(new MessageModel(msg));
            }
            return list;
        }

    }
}