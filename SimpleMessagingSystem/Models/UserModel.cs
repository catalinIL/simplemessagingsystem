﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using SimpleMessagingSystem.CustomAttributes;


namespace SimpleMessagingSystem.Models
{
    /// <summary>
    /// Model for user
    /// </summary>
    public class UserModel
    {
              
        [Required(ErrorMessage="Please enter your Username!")]
        [RegularExpression("^[a-zA-Z-_]+",ErrorMessage="Username can contain only characters and - or _ .")]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name="Username:")]
        [UsernameAvailability(ErrorMessage="This username is not available.")]
        public string Username { get; set; }
        
        
        [Required(ErrorMessage="Please enter your password!")]
        [RegularExpression("^[a-zA-Z0-9]+",ErrorMessage="Please enter only characters and numbers ")]
        [DataType(DataType.Password,ErrorMessage="Please enter only characters and numbers")]
        [StringLength(20,MinimumLength=6,ErrorMessage="The password must contain between 6 and 20 characters")]
        [Display(Name = "Password:")]
        public string Password { get; set; }

        [Required(ErrorMessage="Please confirm your password!")]
        [Compare("Password",ErrorMessage="Passwords do not match!")]
        [Display(Name="Confirm Password:")]
        public string ConfirmPassword { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z ]+", ErrorMessage = "First Name can contain only characters.")]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name="First Name:")]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression("^[a-zA-Z ]+", ErrorMessage = "Last Name can contain only characters.")]
        [DataType(DataType.Text)]
        [StringLength(50)]
        [Display(Name = "Last Name:")]
        public string LastName { get; set; }

        [Required(ErrorMessage="Please enter a date.")]
        [DataType(DataType.Date,ErrorMessage="Please enter a valid date.")]
        [Display(Name = "Date of birth:")]
        public DateTime Date { get; set; }

        [Required]
        [DataType(DataType.Text)]
        [StringLength(1)]
        [Display(Name="Gender:")]
        public string Gender { get; set; }
        
        public string Role { get; set; }

        public UserModel() { }
        public UserModel(DataLayer.User user)
        {
            this.Username = user.Username;
            this.FirstName = user.FirstName;
            this.LastName = user.LastName;
            this.Date = user.Date;
            this.Gender = user.Gender == DataLayer.Gender.M ? "M":"F";
            this.Role = user.Role;
        }

        public static explicit operator DataLayer.User(UserModel obj)
        {
            DataLayer.User output = new DataLayer.User(obj.Username,
                obj.Password, obj.FirstName, obj.LastName, obj.Date, 
                obj.Gender=="1"? DataLayer.Gender.M: DataLayer.Gender.F , obj.Role);
            return output;
        }
    }

    
}