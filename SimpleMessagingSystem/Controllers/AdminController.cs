﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using SimpleMessagingSystem.CustomAttributes;
using SimpleMessagingSystem.Models;
using SimpleMessagingSystem.Models.AdminModels;

namespace SimpleMessagingSystem.Controllers
{
    [CustomAuthorize(Roles="admin")]
    public class AdminController : Controller
    {
        //
        // GET: /Admin/
        /// <summary>
        /// Welcome admin page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {

            int nrUsers = DataLayer.Database.GetNumberOfUsers();
            int nrUnconfUsers = DataLayer.Database.GetNumberOfUnconfirmedUsers();
            int nrMessages = DataLayer.Database.GetNumberOfMessages();

            WelcomePageAdminModel model = new WelcomePageAdminModel(nrUsers, nrUnconfUsers, nrMessages);

            return View(model);
        }

        /// <summary>
        /// Users Page
        /// </summary>
        /// <returns></returns>
        public ActionResult UsersPage()
        {
            var unconfirmedUsers = DataLayer.Database.GetUserRequestsConfirmation();
            var users = DataLayer.Database.GetAllUsers();

            return View(new UsersPageModel(){ UnconfirmedUsers = unconfirmedUsers, Users=users });
        }

        /// <summary>
        /// Messages Page
        /// </summary>
        /// <returns></returns>
        public ActionResult Messages()
        {
            List<DataLayer.Message> messages = DataLayer.Database.GetAllMessages();
         
            return View(MessageModel.GetMessageModel(messages));
        }

        #region actions taken from admin pages
        
        public ActionResult ConfirmUser(int id)
        {
           bool rez = DataLayer.Database.ConfirmUser(id);
           return Json(rez, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DenyUser(int id)
        {
           bool rez = DataLayer.Database.DenyUser(id);
           return Json(rez, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MakeAdmin(int id)
        {
            bool rez = DataLayer.Database.UpdateUserRole(id, "admin");
            return Json(rez, JsonRequestBehavior.AllowGet);
        }

        // Delete user and all messages that involves this user (send, receive)
        public ActionResult DeleteUser(int id)
        {
            bool rez = DataLayer.Database.DeleteUser(id);
            return Json(rez, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
