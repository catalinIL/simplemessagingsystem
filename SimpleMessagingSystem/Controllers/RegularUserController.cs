﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using SimpleMessagingSystem.CustomAttributes;
using SimpleMessagingSystem.Hubs;
using SimpleMessagingSystem.Models;
using SimpleMessagingSystem.Models.RegularUserModels;

namespace SimpleMessagingSystem.Controllers
{
    [CustomAuthorize(Roles="user,admin")]
    public class RegularUserController : Controller
    {
        //
        // GET: /RegularUser/
        /// <summary>
        /// Welcome user page
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            DataLayer.User user = DataLayer.Database.GetUserByUsername(HttpContext.User.Identity.Name);
            int sent = DataLayer.Database.GetNumberOfSentMsgs(user.Id);
            int received = DataLayer.Database.GetNumberOfReceivedMsgs(user.Id);
            int unread = DataLayer.Database.GetNumberOfUnreadMsgs(user.Id);
            
            WelcomePageUserModel model = new WelcomePageUserModel(sent, received, unread);

            return View(model);
        }

        /// <summary>
        /// Inbox page
        /// </summary>
        /// <returns></returns>
        public ActionResult Inbox()
        {
            DataLayer.User user = DataLayer.Database.GetUserByUsername(HttpContext.User.Identity.Name);
            List<DataLayer.Message> messages = DataLayer.Database.GetReceivedMessages(user);

            return View(MessageModel.GetMessageModel(messages));
        }
        
        /// <summary>
        /// Outbox page
        /// </summary>
        /// <returns></returns>
        public ActionResult Outbox()
        {
            DataLayer.User user = DataLayer.Database.GetUserByUsername(HttpContext.User.Identity.Name); 
            List<DataLayer.Message> messages = DataLayer.Database.GetSentMessages(user);

            return View(MessageModel.GetMessageModel(messages));
            
        }

        /// <summary>
        /// Send Message page
        /// </summary>
        /// <returns></returns>
        public ActionResult SendMessage()
        {
            return View();
        }

        #region user actions 

        //effective sending a message, action called from SendMessage page
        public ActionResult SentMessage(string users, string subject, string message)
        {
            if (ModelState.IsValid)
            {
                MatchCollection matches = Regex.Matches(users, @"\(([^)]*)\)");

                DataLayer.User sender = DataLayer.Database.GetUserByUsername(HttpContext.User.Identity.Name);

                try
                {
                    if (matches.Count == 0)
                    {
                        return Json(@"Error. No valid recipient found. 
                                    The username of recipient must be specified between round brackets!"
                                    , JsonRequestBehavior.AllowGet);
                    }

                    foreach (var recipientUsername in matches)
                    {
                        DataLayer.User recipient = DataLayer.Database.GetUserByUsername(
                            recipientUsername.ToString().Replace("(", string.Empty).Replace(")", string.Empty));

                        if (recipient == null)
                        {
                            return Json("Error. Please select a correct recipient!", JsonRequestBehavior.AllowGet);
                        }

                        bool sent = DataLayer.Database.AddMessage(
                            new DataLayer.Message(sender.Id, recipient.Id, subject, DateTime.Now, message));

                        if (!sent)
                        {
                            return Json("Error. Failed to send message to " + recipient.FullName, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch
                {
                    return Json("Error. Failed to send message!", JsonRequestBehavior.AllowGet);
                }
               
                return Json("Success! Message sent.", JsonRequestBehavior.AllowGet);
            }
            return View();
        }

        public ActionResult ResendMessage(string subject, string message)
        {
            SentMessageModel model = new SentMessageModel();
            model.Users = "";
            model.Subject = subject;
            model.Message = message;

            return View("SendMessage", model);
        }

        // used on send message page for dropdown list with all active users 
        public ActionResult GetUsersList()
        {
            List<DataLayer.User> list = DataLayer.Database.GetAllUsers();
            string[] agenda = new string[list.Count];

            for (int i = 0; i < list.Count; i++)
            {
                agenda[i] = list.ElementAt(i).FullName + "(" + list.ElementAt(i).Username + ")";
            }
            return Json(agenda, JsonRequestBehavior.AllowGet);
        }

        public void MarkAsRead(int id)
        {
            DataLayer.Database.MarkMessageAsRead(id);
        }

        public bool DeleteMessage(int messageId)
        {
            return DataLayer.Database.DeleteMessage(messageId);
        }

        #endregion
    }
}
