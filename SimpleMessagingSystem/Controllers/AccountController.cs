﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using DataLayer;
using SimpleMessagingSystem.Hubs;

namespace SimpleMessagingSystem.Controllers
{
    
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        public ActionResult Index()
        {
            if (!HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Login");

            // according to user role the menu will be displayed
            string role = Database.GetUserRole(HttpContext.User.Identity.Name);
            Session["role"] = role;
            Session["username"] = Database.GetUserByUsername(HttpContext.User.Identity.Name).FullName;

            if (role != null && role == "admin")
            {
                return RedirectToAction("Index", "Admin");
            }
            else 
            {
                return RedirectToAction("Index", "RegularUser");
            }
        }

      
        [AllowAnonymous]
        [HttpGet]
        public ActionResult Login()
        {
            if(HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("AlreadyLoggedIn","Error");
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Login(Models.UserModel user)
        {
            DataLayer.User u;
            if (Database.CheckCredentials(user.Username, user.Password, out u))
            {
                FormsAuthentication.SetAuthCookie(u.Username, false);
                                
                Session["username"] = u.FullName;
                Session["role"] = u.Role;

                if (u.Role.Equals("admin"))
                {
                    return RedirectToAction("Index", "Admin");
                }
                else
                {
                    return RedirectToAction("Index", "RegularUser");
                }
            }
          
            return View(user);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Remove("username");
            Session.Remove("role");
            return RedirectToAction("Login", "Account");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult Registration()
        {
            if (HttpContext.User.Identity.IsAuthenticated)
            {
                return RedirectToAction("AlreadyLoggedIn", "Error");
            }

            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult Registration(Models.UserModel user)
        {
            // if input is valid insert user and wait for admin approval for this user
            if (ModelState.IsValid)
            {
                if (Database.InsertUser((User)user))
                {
                    return View("ConfirmRegistration");
                }

            }
            return View(user);
        }

        public static string GetRole(string username)
        {
            return Database.GetUserRole(username);
        }

        public static string GetFullName(string username)
        {
            return Database.GetUserByUsername(username).FullName;
        }
    }

}
