﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SimpleMessagingSystem.CustomAttributes
{
    /// <summary>
    /// Custom attribute that checks if a input string is an available username
    /// </summary>
    public class UsernameAvailability : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string username = (string)value;
            if (DataLayer.Database.CheckUsernameAvailability(username))
            {
                return ValidationResult.Success;
            }
            return new ValidationResult(this.FormatErrorMessage(validationContext.DisplayName));

        }
    }
}