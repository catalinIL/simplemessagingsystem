﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace SimpleMessagingSystem.CustomAttributes
{
    /// <summary>
    /// Custom authorize class for use on roles from personal database
    /// </summary>
    public class CustomAuthorize : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            var roles = Roles.Split(',');
            string username = filterContext.HttpContext.User.Identity.Name;
            
            if (filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                bool auth = false;
                foreach (var role in roles)
                {
                    auth = DataLayer.Database.CheckUserRole(username, role);
                    if(auth)
                    {
                        break;
                    }
                }
                //if has no authorization
                if (!auth)
                {
                    filterContext.Result = new RedirectToRouteResult(new
                        RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
                }
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account", action = "Login" }));
            }
        }
    }
}