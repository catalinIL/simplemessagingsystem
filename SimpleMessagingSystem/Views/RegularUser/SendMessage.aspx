﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SimpleMessagingSystem.Models.RegularUserModels.SentMessageModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Send Message
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Content/themes/base/jquery-ui.css">
    <script src="../../Scripts/jquery-1.8.2.js"></script>
    <script src="../../Scripts/jquery-ui-1.8.24.js"></script>
    <script type="text/javascript">
        //dropdown users list
        function autocompleteUsers() {
                var users;
                $.ajax({
                    url: '<%=Url.Action("GetUsersList","RegularUser")%>',
                    type: 'GET',
                    success: function (data) {
                        users = data;
                        $("#dropdown").autocomplete({
                            source: users
                        });
                    }
                });
        }

        function sentMessage() {
            //check if all field have an input
            if ($.trim($("#dropdown").val()) == '' ||
                $.trim($("#subject").val()) == '' ||
                $.trim($("#message").val()) == '') {
                $("<div>Please enter all information</div>").dialog({
                    resizable: false,
                    width: 300,
                    buttons: {
                        "Close": function () {
                            $(this).dialog("close");
                            
                        }
                    }
                });
                return;
            }

            // ajax call to send message
            $.ajax({
                url: '<%= Url.Action("SentMessage","RegularUser")%>',
                type: 'GET',
                data: {
                    users: $("#dropdown").val(),
                    subject: $("#subject").val(),
                    message: $("#message").val()
                },
                success: function (data) {
                    $("<div>"+data+"</div>").dialog({
                        resizable: false,
                        width: 300,
                        data: data,
                        buttons: {
                            "Close": function () {
                                if (data.toString().indexOf("Success") != -1) {
                                    $("#dropdown").val("");
                                    $("#subject").val("");
                                    $("#message").val("");
                                }
                                $(this).dialog("close");
                            }
                        }
                    });
                },
                error: function (data) {
                    $("<div> <p>Error!</p>" + data + "</div>").dialog({
                        resizable: false,
                        width: 300,
                        data: data,
                        buttons: {
                            "Close": function () {
                                $(this).dialog("close");
                            }
                        }
                    });
                }
            });
        }
    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/SendMessagePage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Send Message</h2>
    
    <table>
        <tr>
            <td class="label"><%= Html.LabelFor(m => m.Users) %></td>
            <td class="field"><%= Html.TextBoxFor(m => m.Users, new { id="dropdown" , onfocus="autocompleteUsers();" })%></td>
        </tr>
        <tr>
            <td colspan="2"><%= Html.ValidationMessageFor(m => m.Users) %></td>
        </tr>
        <tr>
            <td class="label"><%= Html.LabelFor(m => m.Subject) %></td>
            <td class="field"><%= Html.TextBoxFor(m => m.Subject, new { id="subject"})%></td>
        </tr>
        <tr>
            <td colspan="2"><%= Html.ValidationMessageFor(m => m.Subject) %> </td>
        </tr>
        <tr>
            <td colspan="2"><%= Html.ValidationMessageFor(m => m.Message )%></td>
        </tr>
        <tr>
            <td colspan="2"><%= Html.TextAreaFor(m => m.Message, new { id="message"})%></td>
        </tr>
        <tr>
            <td class="ui-button" id="button" colspan="2">
                <button onclick="sentMessage();">Send Message</button>
            </td>
        </tr>
    </table>
</asp:Content>


