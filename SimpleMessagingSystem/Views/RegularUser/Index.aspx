﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SimpleMessagingSystem.Models.RegularUserModels.WelcomePageUserModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Homepage User
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/IndexUserPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <h2>Welcome</h2>
    <div>
        <table>
            <tr>
                <td><%=Html.LabelFor(m => m.NumberOfUnreadMessages) %></td>
                <td class="leftColumn"><%=Html.ValueFor(m => m.NumberOfUnreadMessages) %></td>
            </tr>  
            <tr>
                <td><%=Html.LabelFor(m => m.NumberOfMessagesReceived) %></td>
                <td class="leftColumn"><%=Html.ValueFor(m => m.NumberOfMessagesReceived) %></td>
            </tr>
            <tr>
                <td><%=Html.LabelFor(m => m.NumberOfMessagesSent) %></td>
                <td class="leftColumn"><%=Html.ValueFor(m => m.NumberOfMessagesSent) %></td>
            </tr>
        </table>

    </div>
</asp:Content>


