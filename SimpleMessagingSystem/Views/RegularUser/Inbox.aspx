﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Inbox
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Content/themes/base/jquery-ui.css">
    <script src="../../Scripts/jquery-1.8.2.js"></script>
    <script src="../../Scripts/jquery-ui-1.8.24.js"></script>
    <script src="../../Scripts/jquery.signalR-1.1.4.min.js"></script>
    <script src="~/signalr/hubs"></script>
    <script type="text/javascript">

        // read message function
        function popup(messageId, read) {
            $("#popup").width(750);
            $("#dialog" + messageId).show();

            if (!read) {
                $.ajax({
                    url: '<%=Url.Action("MarkAsRead","RegularUser")%>',
                    type: 'GET',
                    dataType: 'json',
                    cache: false,
                    data: { id: messageId },
                    success: function () {
                        $("#idmsg" + messageId).removeClass("strong");
                    }
                });
            };
            $("#dialog"+messageId).dialog({
                modal: true,
                height: 400,
                width: 800,
                buttons: {
                    Forward: function () {
                        var subj = $("#subj"+messageId).text().trim();
                        var msg = $("#msgValue"+messageId).text().trim();
                        var url= '<%=Url.Action("ResendMessage","RegularUser")%>'+
                                    "?subject="+subj +"&message="+msg;
                        window.location.href = url; 
                        $(this).dialog("close");
                        $("#dialog" + messageId).hide();
                        
                    },
                   
                    Close: function () {
                        $(this).dialog("close");
                        $("#dialog"+messageId).hide();
                    }
                },
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 500
                }
            });
            
        }

        $(function () { // function for live receiving email Not working yet.
            var chat = $.connection.PushMessageHub;
            chat.client.addMessageInbox = function (message) {
                alert(message);
                $('#inboxTable tr:first').after(message);
            }

            $.connection.hub.start();
        });

    </script>
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/InboxPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Inbox</h2>
    <table id="inboxTable">
        <tr>
            <th>Sender Name</th>
            <th>Sent Date</th>
            <th>Subject</th>
        </tr>
        <% foreach(var msg in Model) { %>
        
        <tr <%if(!msg.Read){ %> id="idmsg<%=msg.Id %>" class="strong" <% } %>>

            <td><%= msg.SenderName%></td>
            <td><%= msg.SentDate%></td>
            <td>
                <button class="buttonLink" id="subj<%=msg.Id %>" onclick="popup(<%=msg.Id %>,<%=msg.Read.ToString().ToLower() %>)">
                    <%= msg.Subject%>
                </button>
            </td>
            <td id="dialog<%=msg.Id %>" title="Message" hidden="hidden">    
                <table id="popup">
                    <tr>
                        <td class="tdLeft">
                            <strong>From:</strong> <%= msg.SenderName%>
                        </td>
                        <td class="tdRigth">
                            <strong>Sent Date:</strong> <%= msg.SentDate%>
                        </td>
                    </tr>
                    
                    <tr class="messageRow">
                        <td colspan="2" id="msgValue<%=msg.Id %>">
                            <%=msg.MessageValue %>
                        </td>
                    </tr>
                </table>
                   
            </td>
        </tr>
        <%} %>
    </table>

</asp:Content>
