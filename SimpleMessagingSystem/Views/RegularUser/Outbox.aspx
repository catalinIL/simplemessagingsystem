﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Outbox
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/OutboxPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Content/themes/base/jquery-ui.css">
    <script src="../../Scripts/jquery-1.8.2.js"></script>
    <script src="../../Scripts/jquery-ui-1.8.24.js"></script>
    <script type="text/javascript">
        // read message function
        function popup(messageId) {
            $("#popup").width(750);
            $("#dialog" + messageId).show();
            $("#dialog" + messageId).dialog({
                modal: true,
                height: 400,
                width: 800,
                buttons: {
                    Forward: function () {
                        var subj = $("#subj" + messageId).text().trim();
                        var msg = $("#msgValue" + messageId).text().trim();
                        var url = '<%=Url.Action("ResendMessage","RegularUser")%>' +
                                    "?subject=" + subj + "&message=" + msg;
                        window.location.href = url;
                        $(this).dialog("close");
                        $("#dialog" + messageId).hide();
                    },
                    Close: function () {
                        $(this).dialog("close");
                        $("#dialog" + messageId).hide();
                    }
                },
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 500
                }
            });

        }
        function deleteMessage(messageId) {
            $.ajax({
                url: '<%=Url.Action("DeleteMessage","RegularUser")%>',
                type: 'GET',
                dataType: 'json',
                cache: false,
                data: { id: messageId },
                success: function (rez) {
                    if (rez) {
                        var rowId = 'idmsg' + messageId;
                        $('#' + rowId).remove();
                    }
                },
                error: function () {
                    alert("Some error has occurred.")
                }
            });
        }
    </script>

</asp:Content>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Outbox</h2>
    <table>
        <tr>
            <th>Receiver Name</th>
            <th>Send Date</th>
            <th>Subject</th>
            <th>Action</th>
        </tr>
        <% foreach(var msg in Model) { %>
        <tr>
            <td><%= msg.ReceiverName%></td>
            <td><%= msg.SentDate%></td>
            <td id="idmsg<%=msg.Id %>">
                <button class="buttonLink" id="subj<%=msg.Id %>" onclick="popup(<%=msg.Id %>)"><%= msg.Subject%></button>
            </td>
            <td><button onclick="deleteMessage(<%=msg.Id %>);">Delete</button></td>
            <td id="dialog<%=msg.Id %>" title="Message" hidden="hidden">    
                <table id="popup">
                    <tr>
                        <td class="tdLeft">
                            <strong>To:</strong> <%= msg.ReceiverName%>
                        </td>
                        <td class="tdRigth">
                            <strong>Sent Date:</strong> <%= msg.SentDate%>
                        </td>
                    </tr>
                    
                    <tr class="messageRow">
                        <td colspan="2" id="msgValue<%=msg.Id %>">
                            <%=msg.MessageValue %>
                        </td>
                    </tr>
                </table>
                   
            </td>
        </tr>
        <%} %>
    </table>

</asp:Content>


