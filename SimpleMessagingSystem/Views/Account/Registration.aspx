﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SimpleMessagingSystem.Models.UserModel>" %>

<asp:Content ID="Content" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Content/themes/base/jquery-ui.css">
    <script src="../../Scripts/jquery-1.8.2.js"></script>
    <script src="../../Scripts/jquery-ui-1.8.24.js"></script>
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script>
         $(function () {
             $("#datepicker").datepicker({ minDate: new Date(1970,01,01), maxDate: new Date() });
         });
    </script>
</asp:Content>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Registration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/RegisterPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="form">
        <% using (Html.BeginForm()){%>
       
            <%= Html.ValidationSummary(true) %>
            <fieldset>
                <legend>Registration</legend>
                <div>
                    <%= Html.LabelFor(u => u.Username, new { @class="label" }) %> 
                    <%= Html.TextBoxFor(u => u.Username, new { @class="text" }) %>
                    <%= Html.ValidationMessageFor(u => u.Username) %>
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.Password, new { @class="label" }) %> 
                    <%= Html.PasswordFor(u => u.Password, new { @class="text" }) %>
                    <%= Html.ValidationMessageFor(u => u.Password) %>
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.ConfirmPassword, new { @class="label" }) %> 
                    <%= Html.PasswordFor(u => u.ConfirmPassword, new { @class="text" }) %>
                    <%= Html.ValidationMessageFor(u => u.ConfirmPassword) %>
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.FirstName, new { @class="label" }) %> 
                    <%= Html.TextBoxFor(u => u.FirstName, new { @class="text" }) %>
                    <%= Html.ValidationMessageFor(u => u.FirstName) %>
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.LastName, new { @class="label" }) %> 
                    <%= Html.TextBoxFor(u => u.LastName, new { @class="text" }) %>
                    <%= Html.ValidationMessageFor(u => u.LastName) %>
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.Date, new { @class="label" }) %> 
                    <%= Html.TextBoxFor(u => u.Date, new { id="datepicker" , @class="text"}) %>
                    <%= Html.ValidationMessageFor(u => u.Date) %>
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.Gender, new { @class="label" }) %> 
                    <%= Html.RadioButtonFor(u => u.Gender,"1", new { @class="radioBtn" }) %>
                    <%= Html.LabelFor(u=> u.Gender, "Male") %>
                    <%= Html.RadioButtonFor(u => u.Gender, "2", new { @class="radioBtn" })%>
                    <%= Html.LabelFor(u=> u.Gender, "Female") %> <br />
                    <%= Html.ValidationMessageFor(u => u.Gender) %>
                </div><br />
                <input type="submit" value="Register" id="submitButton" />
            </fieldset>
        <% } %>
        <div>
            <%= Html.ActionLink("I have an account", "Login","Account") %>
        </div>
    </div>
</asp:Content>

