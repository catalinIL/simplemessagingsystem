﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Confirm Registration
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopPageContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Confirm Registration</h2>
<p>
    You have successfully register. Your request is being processed and you will be able to use your account after an admin accepts your request.
    Thank you!
</p>
<%= Html.ActionLink("Go to home page", "Login","Account") %>
</asp:Content>
