﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SimpleMessagingSystem.Models.UserModel>" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Login
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopPageContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>

<asp:Content ID="Content5" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/LoginPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="form">
        <% using (Html.BeginForm()){%>
       
            <fieldset>
                <legend>Login</legend>
                <div>
                    <%= Html.LabelFor(u => u.Username, new { @class="label" })%> 
                    <%= Html.TextBoxFor(u => u.Username, new { @class="text" }) %>
                
                </div><br />
                <div>
                    <%= Html.LabelFor(u => u.Password, new { @class="label" }) %> 
                    <%= Html.PasswordFor(u => u.Password, new { @class="text" }) %>
                
                </div><br />
                <input type="submit" value="Log In" id="submitButton" />
                <%= Html.ValidationSummary(true,@"Invalid credentials! 
                                                Please check your Username and Password!") %>
            </fieldset>
        <% } %>
        <div>
            <%= Html.ActionLink("Register", "Registration","Account") %>
        </div>
    </div>
</asp:Content>

