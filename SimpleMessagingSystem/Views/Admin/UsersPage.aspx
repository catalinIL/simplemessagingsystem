﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Users Page
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/UsersPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
     <script src="../../Scripts/jquery-1.8.2.js"></script>
    <script type="text/javascript" >
        function confirm(userId){
            $.ajax({
                url:'<%=Url.Action("ConfirmUser","Admin")%>',
                type:'GET',
                dataType:'json',
                cache: false,
                data:{id: userId},
                success: function (rez) {
                    if (rez) {
                        var rowId = 'unconf' + userId;
                        $('#' + rowId + ' .button').remove();
                        $('#' + rowId).append('<td colspan="2">Just confirmed</td>');
                        $('#usersTable tr:first').after($('#' + rowId));
                    }
                },
                error: function(){
                    alert("Some error has occurred.")
                }
            });
        }
        function deny(userId) {
            $.ajax({
                url: '<%=Url.Action("DenyUser","Admin")%>',
                type: 'GET',
                dataType: 'json',
                cache: false,
                data: { id: userId },
                success: function (rez) {
                    if (rez) {
                        var rowId = 'unconf' + userId;
                        $('#' + rowId).remove();
                    }
                },
                error: function () {
                    alert("Some error has occurred.")
                }
            });
        }

        function makeAdmin(userId) {
            $.ajax({
                url: '<%=Url.Action("MakeAdmin","Admin")%>',
                type: 'GET',
                dataType: 'json',
                cache: false,
                data: { id: userId },
                success: function (rez) {
                    if (rez) {
                        var rowId = 'role' + userId;
                        $('#' + rowId).html('admin');
                    }
                },
                error: function () {
                    alert("Some error has occurred.")
                }
            });
        }

        function deleteUser(userId) {
            $.ajax({
                url: '<%=Url.Action("DeleteUser","Admin")%>',
                type: 'GET',
                dataType: 'json',
                cache: false,
                data: { id: userId },
                success: function (rez) {
                    if (rez) {
                        var rowId = 'user' + userId;
                        $('#' + rowId).remove();
                    }
                },
                error: function () {
                    alert("Some error has occurred.")
                }
            });
        }
    </script>
   
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Users Page</h2>
    <%if (Model.UnconfirmedUsers.Count > 0){ %>
    <h3>Unconfirmed users</h3>
    <table>
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
            <th>Gender</th>
            <th>Role</th>
            <th colspan="2" class="actionButtons">Confirmation</th>
        </tr>
        <% foreach(var user in Model.UnconfirmedUsers) { %>
        <tr id="unconf<%=user.Id %>">
            <td><%=user.Id %></td>
            <td><%=user.Username %></td>
            <td><%=user.FirstName %></td>
            <td><%=user.LastName %></td>
            <td><%=user.Date.ToString("MM/dd/yyyy") %></td>
            <td><%=user.Gender %></td>
            <td><%=user.Role %></td>
            <td class="button"><button onclick="confirm(<%=user.Id %>);">Confirm</button></td>
            <td class="button"><button onclick="deny(<%=user.Id %>);">Deny</button></td>
        </tr>
        <%} %>
    </table>
    <%} %>

    <br />
    <h3>Confirmed users</h3>
    <table id="usersTable">
        <tr>
            <th>Id</th>
            <th>Username</th>
            <th>First name</th>
            <th>Last name</th>
            <th>Date of birth</th>
            <th>Gender</th>
            <th>Role</th>
            <th colspan="2" class="actionButtons">Actions</th>
        </tr>
        <% foreach(var user in Model.Users) { %>
        <tr id="user<%=user.Id %>">
            <td><%=user.Id %></td>
            <td><%=user.Username %></td>
            <td><%=user.FirstName %></td>
            <td><%=user.LastName %></td>
            <td><%=user.Date.ToString("MM/dd/yyyy") %></td>
            <td><%=user.Gender %></td>
            <td id="role<%=user.Id %>"><%=user.Role %></td>
            <td><button onclick="makeAdmin(<%=user.Id %>);">Make Admin</button></td>
            <td><button onclick="deleteUser(<%=user.Id %>);">Delete</button></td>
        </tr>
        <%} %>
    </table>

</asp:Content>


