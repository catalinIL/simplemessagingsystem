﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Messages
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/MessagesPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ScriptContent" runat="server">
    <link rel="stylesheet" href="../../Content/themes/base/jquery-ui.css">
    <script src="../../Scripts/jquery-1.8.2.js"></script>
    <script src="../../Scripts/jquery-ui-1.8.24.js"></script>
    <script type="text/javascript">
        function popup(messageId) {
            $("#popup").width(750);
            $("#dialog" + messageId).show();
            $("#dialog"+messageId).dialog({
                modal: true,
                height: 400,
                width: 800,
                buttons: {
                    Close: function () {
                        $(this).dialog("close");
                        $("#dialog" + messageId).hide();

                    }
                },
                show: {
                    effect: "blind",
                    duration: 1000
                },
                hide: {
                    effect: "explode",
                    duration: 500
                }
            });
            
        }
       
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Messages</h2>
    <table>
        <tr>
            <th>Sender Name</th>
            <th>Receiver Name</th>
            <th>Send Date</th>
            <th>Subject</th>
        </tr>
        <% foreach(var msg in Model) { %>
        <tr>
            <td><%= msg.SenderName%></td>
            <td><%= msg.ReceiverName%></td>
            <td><%= msg.SentDate%></td>
            <td id="idmsg<%=msg.Id %>">
                <button class="buttonLink" onclick="popup(<%=msg.Id %>)"><%= msg.Subject%></button>
            </td>
            <td id="dialog<%=msg.Id %>" title="Message" hidden>    
                <table id="popupMessage">
                    <tr>
                        <td class="tdLeft">
                            <strong>From:</strong> <%= msg.SenderName%>
                        </td>
                        <td class="tdRigth">
                            <strong>Sent Date:</strong> <%= msg.SentDate%>
                        </td>
                    </tr>
                    <tr>
                        <td class="tdLeft">
                            <strong>To:</strong> <%= msg.ReceiverName%>
                        </td>
                        <td class="tdRigth">
                            <strong>Message Id:</strong> <%=msg.Id %>
                        </td>
                    </tr>
                    <tr class="messageRow">
                        <td colspan="2">
                            <%=msg.MessageValue %>
                        </td>
                    </tr>
                </table>
                   
            </td>
        </tr>
        <%} %>
    </table>

   
</asp:Content>
