﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<SimpleMessagingSystem.Models.AdminModels.WelcomePageAdminModel>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Index
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CustomStyleContent" runat="server">
    <link href="../../Content/IndexAdminPage.css" rel="stylesheet" />
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <h2>Welcome</h2>

    <table>
        <tr>
            <td><%=Html.LabelFor(m => m.NumberOfUsers) %></td>
            <td class="leftColumn"><%=Html.ValueFor(m => m.NumberOfUsers) %></td>
        </tr>
        <tr>
            <td><%=Html.LabelFor(m => m.NumberOfUnconfirmedUsers) %></td>
            <td class="leftColumn"><%=Html.ValueFor(m => m.NumberOfUnconfirmedUsers) %></td>
        </tr>
        <tr>
            <td><%=Html.LabelFor(m => m.NumberOfMessages) %></td>
            <td class="leftColumn"><%=Html.ValueFor(m => m.NumberOfMessages) %></td>
        </tr>
    </table>
</asp:Content>


