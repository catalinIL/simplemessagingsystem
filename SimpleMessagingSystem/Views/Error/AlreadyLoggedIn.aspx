﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Site.Master" Inherits="System.Web.Mvc.ViewPage<dynamic>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
    Already Logged In
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="TopPageContent" runat="server">
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MenuContent" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<h2>Already Logged In</h2>
    <div>
        <h4>
            You are logged in! If you want to login again or register
            please <%= Html.ActionLink("logout", "Logout","Account") %> first.
        </h4>
    </div>
</asp:Content>
