﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Microsoft.AspNet.SignalR;
using SimpleMessagingSystem.CustomAttributes;

namespace SimpleMessagingSystem.Hubs
{
    
    [CustomAuthorize(Roles = "user,admin")]
    // not working yet :(
    // useful for live notification on receiving an email
    public class PushMessageHub : Hub
    {
        public void Send(string who, string message)
        {
            Clients.Group(who).addMessageInbox(message);
        }

        public override Task OnConnected()
        {
            string name = Context.User.Identity.Name;

            Groups.Add(Context.ConnectionId, name);

            return base.OnConnected();
        }
    }

    
}