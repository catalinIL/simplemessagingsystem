The connection string to database is in Database.cs file in DataLayer project.

The already registered users have password created by duplicating user name
ex: Username: admin Password adminadmin
	Username: user Password useruser etc.
excepting for the username: test that have the password: test 

In console DataLayer is defined a class for manipulating database,
And two classes that represents user and message from database,
In database, users that are registered but not confirmed are stored in a separate table.
I have used ADO.NET in classic mode (although I prefer EF, which I have used more 
in school projects but at Used technologies it was mentioned ADO.NET so I stick only with it).

For login purpose I used FormsAuthentication, and to constrain access I defined a 
custom authorization attribute because I what to use roles from my table and not from 
ASP.NET Membership.

In SendMessage page to view active user which can receive a message a character must be
enter that is a part form that username, last name or first name of recipient user.
A message can be send to multiple users if their username are mentioned between brackets
and are separated by a comma. Ex: firstName lastName(test), fnPaul lnPaul(Paul) .

I tested this project on Chrome and Firefox. 

The rest of the project is self explanatory.

Thank you,