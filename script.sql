USE [master]
GO
/****** Object:  Database [SimpleMessagingSystemDB]    Script Date: 7/24/2014 3:27:54 PM ******/
CREATE DATABASE [SimpleMessagingSystemDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SimpleMessagingSystemDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SimpleMessagingSystemDB.mdf' , SIZE = 3072KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SimpleMessagingSystemDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL11.MSSQLSERVER\MSSQL\DATA\SimpleMessagingSystemDB_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET COMPATIBILITY_LEVEL = 110
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SimpleMessagingSystemDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET AUTO_CREATE_STATISTICS ON 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET RECOVERY FULL 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET  MULTI_USER 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SimpleMessagingSystemDB', N'ON'
GO
USE [SimpleMessagingSystemDB]
GO
/****** Object:  Table [dbo].[Message]    Script Date: 7/24/2014 3:27:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Message](
	[MessageId] [int] IDENTITY(1,1) NOT NULL,
	[From] [int] NOT NULL,
	[To] [int] NOT NULL,
	[Subject] [nvarchar](50) NOT NULL,
	[SentDate] [datetime] NOT NULL,
	[ReceivedDate] [datetime] NOT NULL,
	[Message] [nvarchar](max) NOT NULL,
	[Read] [bit] NOT NULL,
 CONSTRAINT [PK_Message] PRIMARY KEY CLUSTERED 
(
	[MessageId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserData]    Script Date: 7/24/2014 3:27:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserData](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Salt] [binary](4) NOT NULL,
	[Password] [binary](64) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[Role] [nchar](5) NOT NULL,
 CONSTRAINT [PK_UserData] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UserData] UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserPendingApproval]    Script Date: 7/24/2014 3:27:57 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserPendingApproval](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [nvarchar](50) NOT NULL,
	[Salt] [binary](4) NOT NULL,
	[Password] [binary](64) NOT NULL,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[BirthDate] [date] NOT NULL,
	[Gender] [tinyint] NOT NULL,
	[Role] [nchar](5) NOT NULL,
 CONSTRAINT [PK_UserPendingApproval] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_UserPendingApproval] UNIQUE NONCLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
ALTER TABLE [dbo].[Message] ADD  CONSTRAINT [DF_Message_Read]  DEFAULT ((0)) FOR [Read]
GO
ALTER TABLE [dbo].[UserData] ADD  CONSTRAINT [DF_User_Role]  DEFAULT ('user') FOR [Role]
GO
ALTER TABLE [dbo].[UserPendingApproval] ADD  CONSTRAINT [DF_UserPendingApproval_Role]  DEFAULT ('user') FOR [Role]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_UserData] FOREIGN KEY([From])
REFERENCES [dbo].[UserData] ([UserId])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_UserData]
GO
ALTER TABLE [dbo].[Message]  WITH CHECK ADD  CONSTRAINT [FK_Message_UserData1] FOREIGN KEY([To])
REFERENCES [dbo].[UserData] ([UserId])
GO
ALTER TABLE [dbo].[Message] CHECK CONSTRAINT [FK_Message_UserData1]
GO
USE [master]
GO
ALTER DATABASE [SimpleMessagingSystemDB] SET  READ_WRITE 
GO
